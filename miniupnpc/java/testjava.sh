#!/bin/bash

JAVA=java
JAVAC=javac
CP=$(for i in *.jar; do echo -n $i:; done).

# you have to include natives manualy in most cases.
# or rebuild jar without -noLibBundle option
export LD_LIBRARY_PATH=$(PWD)/../

$JAVAC -cp $CP JavaBridgeTest.java || exit 1
$JAVA -cp $CP JavaBridgeTest 12345 UDP || exit 1
